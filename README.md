# OpenML dataset: superconduct

https://www.openml.org/d/43174

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset Description**

The data contains information on 21263 superconductors. The first 81 columns contain extracted
features and the 82nd column contains the critical temperature which is used as the target
variable. The original data from which the features were extracted comes from
http://supercon.nims.go.jp/index_en.html, which is public.

**Related studies**

https://www.frontiersin.org/articles/10.3389/fmats.2021.714752/full

**Source**

[UCI](https://archive.ics.uci.edu/ml/datasets/superconductivty+data) (2018)

**Citation**

Please cite [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43174) of an [OpenML dataset](https://www.openml.org/d/43174). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43174/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43174/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43174/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

